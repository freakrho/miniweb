import logging
import os

from miniweb import MiniWeb
import time

if __name__ == "__main__":
    logging.basicConfig(filename="log.txt",
                        filemode='a',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.DEBUG)
    web = MiniWeb()
    web.start()
    time.sleep(3)
    print("now let's change to root")
    web.dir = "root"
    web.start()