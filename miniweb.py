import http.server
import socketserver
import threading
import time
import logging


class MiniWeb:
    def __init__(self, port: int = 0, dir : str = ""):
        self.port = port
        self.dir = dir
        self._stop = False
        self._serve_thread = None
        self._httpd = None
        self._logger = logging.getLogger(__name__)

    def _get_handler(self):
        def _init(self, *args, **kwargs):
            return http.server.SimpleHTTPRequestHandler.__init__(self, *args, directory=self.directory, **kwargs)

        return type(f'HandlerFrom<{self.dir}>',
                    (http.server.SimpleHTTPRequestHandler,),
                    {'__init__': _init, 'directory': self.dir})

    def stop(self):
        self._stop = True
        self._httpd.server_close()

        while self._serve_thread.is_alive():
            time.sleep(0.1)
        self._logger.info("Server stopped")

    def start(self):
        if  self._serve_thread != None and self._serve_thread.is_alive():
            self.stop()

        self._stop = False
        self._serve_thread = threading.Thread(target=self._serve)
        self._serve_thread.start()

    def _serve(self):
        if not self.dir.strip():
            handler = http.server.SimpleHTTPRequestHandler
        else:
            handler = self._get_handler()

        self._httpd = socketserver.TCPServer(("", self.port), handler)
        self.port = self._httpd.server_address[1]
        self._logger.info("Server started at port %s" % self.port)
        while not self._stop:
            self._httpd.handle_request()

